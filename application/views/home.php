<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="assets/img/home/carousel1-mini.jpg" alt="ISAFIS 1">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/img/home/carousel2-mini.jpg" alt="ISAFIS 2">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<section id="programs">
  <div class="container">
    <h1 class="section-heading">PROGRAMS</h1>

    <div class="row">
      <div class="w3-card mx-auto p-4">
        <p>Lorem ipsum dolor sit amet, nullam cetero option vel id, cu assum qualisque ius. Pro in nobis diceret erroribus. Latine ocurreret dignissim nec cu. Vel te falli audire vivendo, ad pro dolore disputationi.</p>
      </div>

      <div class="w3-card mx-auto p-4">
        <p>Lorem ipsum dolor sit amet, nullam cetero option vel id, cu assum qualisque ius. Pro in nobis diceret erroribus. Latine ocurreret dignissim nec cu. Vel te falli audire vivendo, ad pro dolore disputationi.</p>
      </div>

      <div class="w3-card mx-auto p-4">
        <p>Lorem ipsum dolor sit amet, nullam cetero option vel id, cu assum qualisque ius. Pro in nobis diceret erroribus. Latine ocurreret dignissim nec cu. Vel te falli audire vivendo, ad pro dolore disputationi.</p>
      </div>
    </div>

    <div class="row second-row">
      <div class="w3-card mx-auto p-4">
        <p>Lorem ipsum dolor sit amet, nullam cetero option vel id, cu assum qualisque ius. Pro in nobis diceret erroribus. Latine ocurreret dignissim nec cu. Vel te falli audire vivendo, ad pro dolore disputationi.</p>
      </div>

      <div class="w3-card mx-auto p-4">
        <p>Lorem ipsum dolor sit amet, nullam cetero option vel id, cu assum qualisque ius. Pro in nobis diceret erroribus. Latine ocurreret dignissim nec cu. Vel te falli audire vivendo, ad pro dolore disputationi.</p>
      </div>
    </div>

    <div class="row">
      <button class="learn-more mx-auto px-3 py-2">Learn more</button>
    </div>
  </div>
</section>

<section id="news">
  <div class="container">
    <h1 class="section-heading">NEWS</h1>

    <div class="row">
      <div class="w3-card mx-auto p-4">
        <p>Lorem ipsum dolor sit amet, nullam cetero option vel id, cu assum qualisque ius. Pro in nobis diceret erroribus. Latine ocurreret dignissim nec cu. Vel te falli audire vivendo, ad pro dolore disputationi.</p>
      </div>

      <div class="w3-card mx-auto p-4">
        <p>Lorem ipsum dolor sit amet, nullam cetero option vel id, cu assum qualisque ius. Pro in nobis diceret erroribus. Latine ocurreret dignissim nec cu. Vel te falli audire vivendo, ad pro dolore disputationi.</p>
      </div>

      <div class="w3-card mx-auto p-4">
        <p>Lorem ipsum dolor sit amet, nullam cetero option vel id, cu assum qualisque ius. Pro in nobis diceret erroribus. Latine ocurreret dignissim nec cu. Vel te falli audire vivendo, ad pro dolore disputationi.</p>
      </div>
    </div>

    <div class="row">
      <button class="learn-more mx-auto px-3 py-2">Learn more</button>
    </div>
  </div>
</section>

<section id="contact">
  <div class="container">
    <h1 class="section-heading">CONTACT US</h1>

    <form class="contact mx-auto">
      <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
      </div>
      <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" id="name" type="text" placeholder="Enter name">
      </div>
      <div class="form-group">
        <label for="message">Message</label>
        <textarea class="form-control" id="message" rows="3"></textarea>
      </div>
      <div class="row">
        <button type="submit" class="learn-more mx-auto px-3 py-2">Send</button>
      </div>
    </form>
  </div>
</section>
