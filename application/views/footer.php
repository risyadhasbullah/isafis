</div>
<section id="section-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
        <ul>
          <li>Home</li>
					<li><a href="/#programs">Programs</a></li>
          <li><a href="/#news">News</a></li>
          <li><a href="/#contact">Contact us</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul>
          <li>About</li>
          <li>Organization</li>
          <li>Structure</li>
          <li>Milestones</li>
          <li>Delegations</li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul>
          <li>Programs</li>
          <li>IIW</li>
          <li>ISAFIS Journal</li>
          <li>UN4MUN</li>
          <li>USEAC</li>
          <li>Jakarta UN4MUN</li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul class="social-media">
          <li>Find us on</li>
          <li><a class="instagram" href="https://www.instagram.com/isafis_official"><i class="fab fa-instagram"></i></a></li>
          <li><a class="facebook" href="https://www.facebook.com/ISAFIS-Indonesian-Student-Association-for-International-Studies-1532157867030680/"><i class="fab fa-facebook"></i></a></li>
          <li><a class="twitter" href="https://twitter.com/ISAFIS_official"><i class="fab fa-twitter"></i></a></li>
          <li><a class="linkedin" href="https://www.linkedin.com/company/isafis/"><i class="fab fa-linkedin"></i></a></li>
        </ul>
      </div>
		</div>
	</div>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>

</body>
</html>
