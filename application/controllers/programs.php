<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Programs extends CI_Controller {
	public function index()
	{
		$this->load->helper('html');
		$this->load->view('head');
		$this->load->view('programs_head');
		$this->load->view('navbar');
		$this->load->view('programs');
		$this->load->view('footer');
	}
}
